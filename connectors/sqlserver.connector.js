

const SqlServer = require("msnodesqlv8");
const baseConnector = require('./base.connector');
const commonMethods = require('../lib/commonmethods.js');


function buildConnectionString(connectiondetail) {
    var connectionString =  "";

    if (connectiondetail) {
        var conn_sqlserver =  connectiondetail.server;
        if (!commonMethods.isNull(connectiondetail.instancename)) {
            conn_sqlserver = conn_sqlserver + "\\" + connectiondetail.instancename;
        }
        if (!commonMethods.isNull(connectiondetail.port)) {
        //  conn_sqlserver = conn_sqlserver + "," + connectiondetail.port;
        }

        connectionString =  "Driver={SQL Server Native Client 11.0};" + "server=" +  conn_sqlserver + ";Database=" + connectiondetail.database + ";";
                                    
        if (connectiondetail.auth && connectiondetail.auth.toLowerCase() == 'win') {
            connectionString = connectionString + "Trusted_Connection=Yes;"
        }
        else {
            connectionString = connectionString + "Uid=" + connectiondetail.user + ";Pwd=" + connectiondetail.password + ";";
        }
    }
                          
    return connectionString;
}

// connect to your database
function executeSQL(connectiondetail, sqlQuery, callback) {

    //building connection string
    var connectionString = buildConnectionString(connectiondetail);
    console.log('connection string...', connectionString);

    //execute the query
     SqlServer.query(connectionString, sqlQuery, (err, rows) => {
        if (err) {
            console.log(err);
            callback({ message:'Error while executing sql on SQL Server', details: err}, null);
        }
        else {
            callback(null,rows);  
        }
    });
 }

 function connectDB(connectiondetail, callback) {
    
        //building connection string
        var connectionString = buildConnectionString(connectiondetail);
        console.log('connection string...', connectionString);

        //connect
        SqlServer.open(connectionString, function (err, conn) {

            if (err) {
                callback({
                    "success": false,
                    "error": err
                });
            }
            else {
                
                callback({
                    "success": true,
                    "error": null
                });
            }
        });

};

 function executeProcedure(connectiondetail, procedurename, procparams, filtercriteria, maxrows, callback) {
    
        //building connection string
        var connectionString = buildConnectionString(connectiondetail);
        console.log('connection string...', connectionString);

        var params = [];
        
        if (procparams) {
          
            for(var pCtr=0; pCtr<procparams.length; pCtr++) {
                var matched = false;
              
                for(var i=0;i< Object.keys(filtercriteria).length; i++) {
                    var key = Object.keys(filtercriteria)[i];
                    var val = filtercriteria[key];
              
                    if (procparams[pCtr].toLowerCase() == ('#'+key.toLowerCase() + '#')) {
                        params.push(val);
                        matched = true;
                        break;
                    }
                }
                if (!matched) {
                    params.push(null);
                }
            }
        }

        console.log(params,procparams,procparams.length,filtercriteria);

        //call the procedure
        SqlServer.open(connectionString, function (err, conn) {
            var pm = conn.procedureMgr();
            pm.callproc(procedurename, params, 
                function(err, results, output) {
                    if (err) {
                        console.log(err);
                        callback({ message:'Error while executing procedure on SQL Server', details: err}, null);
                    }
                    else {
                        var data = [];

                        if (results) {
                            if (maxrows > 0 && results.length > maxrows) {
                                for(var i=0; i<maxrows; i++) {
                                    data.push(results[i]);
                                }
                            }
                            else {
                                data = results;
                            }
                            callback(null,data);      
                        }
                        else {
                            callback({ message:'Error while executing procedure on SQL Server, result is null', 
                            details: results, output: output}, null);
                        }
                        
                    }
            });
        });

};

function getSQL(entitydetail, filtercriteria, maxrows) 
{
    console.log(entitydetail, filtercriteria, maxrows);
    var sqlstring = "";

    if (!commonMethods.isNull(entitydetail.source.sql)) {
        sqlstring = entitydetail.sql;
    }
    else {

        if (!commonMethods.isNull(entitydetail.source.table)) {

            sqlstring = "SELECT ";

            if (!commonMethods.isNull(maxrows)) {
                sqlstring = sqlstring + "TOP " + maxrows + " ";
            }

            if (!commonMethods.isNull(entitydetail.source.columns)) { 
                sqlstring = sqlstring + entitydetail.source.columns;
            }
            else {
                sqlstring = sqlstring + " * ";
            }

            sqlstring = sqlstring + " FROM " + entitydetail.source.table + " WHERE ";

            if (!commonMethods.isNull(entitydetail.source.filter)) {
                sqlstring = sqlstring + entitydetail.source.filter;
            }
            else {
                sqlstring = sqlstring + " 1=1 ";
            }
        }
    }

    if (!commonMethods.isNull(sqlstring) && !commonMethods.isNull(filtercriteria)) {
        var conditioncols = baseConnector.buildSQLCondition(filtercriteria);
        if (!commonMethods.isNull(conditioncols)) {
            sqlstring = sqlstring + " AND " + conditioncols;
        }
    }
    
    return sqlstring;
 }
    

module.exports = {
    About: function(callback) {
                callback("This is SQL Server Connector defined to pull the data from MS SQL Server.");
    },

    TestConnection: function(connectiondetail, callback) {
                        connectDB(connectiondetail, callback);
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {
       
                    var sqlstring = getSQL(entityinfo.dataset, filtercriteria, maxrows);

                    if (!commonMethods.isNull(sqlstring)) {
                      
                        executeSQL(entityinfo.connector.connectiondetail, sqlstring, function(err, data) {

                             if (err) {
                                 callback(err,null);   
                             }   
                             else {
                                 callback(null,data);
                             }
                        });
                    }
                    else {

                        if (!commonMethods.isNull(entityinfo.dataset.source) && 
                            !commonMethods.isNull(entityinfo.dataset.source.procedure) && 
                            !commonMethods.isNull(entityinfo.dataset.source.procedure.name)) {
                            
                              executeProcedure(entityinfo.connector.connectiondetail, 
                                         entityinfo.dataset.source.procedure.name, 
                                         entityinfo.dataset.source.procedure.parameters, 
                                         filtercriteria, maxrows,
                                         function(err, data) {
      
                                   if (err) {
                                       callback(err,null);   
                                   }   
                                   else {
                                       callback(null,data);
                                   }
                              });
                          }
                          else {

                                callback({message:'SQL String is formed as blank',
                                        details: {"entityinfo": entityinfo,
                                                    "filtercriteria": filtercriteria}  
                                        },null);
                        }
                    }
    }         


}