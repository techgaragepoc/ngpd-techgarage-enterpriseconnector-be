
const request = require('request');
const baseConnector = require('./base.connector');
const commonMethods = require('../lib/commonmethods.js');

function getAbsoluteUrl(baseurl, relativeurl) {

    var url = baseurl;

    if (baseurl.substr(-1) == '/') {
        url = baseurl.substr(0,baseurl.length-1);
    }

    if (relativeurl.length>0) {
        if (relativeurl.substr(0,1) == '/') {
            url = url + relativeurl;
        }
        else {
            url = url + '/' + relativeurl;
        }
    } 
    
    return url;

}

function replaceUrlParams(source, filtercriteria) {
    if (typeof filtercriteria === 'object' && !commonMethods.isEmpty(filtercriteria)) {
          //  && typeof filtercriteria.url === 'object' && !commonMethods.isEmpty(filtercriteria.url)) {
            var urlParams = filtercriteria; //filtercriteria.url;

            for(var i=0; i<Object.keys(urlParams).length;i++) {
                var key = Object.keys(urlParams)[i];
                var param = '#'+key + '#';
                if (source.indexOf(param) > -1) {
                    source = commonMethods.replaceAll(source, param, urlParams[key]);
                }
            }
    }

    return source;
}

function replaceHeaderParams(source, filtercriteria) {
    
    if (typeof filtercriteria === 'object' && !commonMethods.isEmpty(filtercriteria)) {
       
        if (
            // typeof filtercriteria.headers === 'object' && !commonMethods.isEmpty(filtercriteria.headers) &&
             typeof source === 'object' && !commonMethods.isEmpty(source)) {

            var hdrParams = filtercriteria; //filtercriteria.headers;
            
                for(var i=0; i<Object.keys(hdrParams).length;i++) {
                    var paramkey = Object.keys(hdrParams)[i];
                    var paramval = hdrParams[paramkey];
                    
                    for(var j=0; j<Object.keys(source).length;i++) {
                        var srckey = Object.keys(source)[j];

                        source[srckey] = paramval;
                    }
                }
        } //for headers

     }

     return source;
}


function connectDB(connectiondetail, callback) {
    
        var combinedHeaders = {};
    
        combinedHeaders = commonMethods.addPropertyValuePair(combinedHeaders,connectiondetail.headers);
       // combinedHeaders = commonMethods.addPropertyValuePair(combinedHeaders,sessionToken);
    
        var dataurl = getAbsoluteUrl(connectiondetail.baseurl, '');
    
        var options = {
            method: 'GET',
            url: dataurl,
            headers: combinedHeaders,
        };
    
        console.log('request options: ', options);
    
        if (connectiondetail.proxy && connectiondetail.proxy!='undefined') {
            options["proxy"] = connectiondetail.proxy;
        }
    
        request(options, function(err, response, body) {
    
            if (err) {
                callback({
                    "success": false,
                    "error": err
                });
            }
            else {
                callback({
                    "success": true,
                    "error": null
                });
            }
    
        });
            
    };

// connect to your database and get documents
function getResponse(entityinfo, sessionToken, filtercriteria, maxrows, callback) {

    var connectiondetail = entityinfo.connector.connectiondetail;
    var entitydetail = entityinfo.dataset.source;
    var combinedHeaders = {};

    combinedHeaders = commonMethods.addPropertyValuePair(combinedHeaders,connectiondetail.headers);
    combinedHeaders = commonMethods.addPropertyValuePair(combinedHeaders,entitydetail.headers);

    var dataurl = getAbsoluteUrl(connectiondetail.baseurl, entitydetail.url);

    dataurl = replaceUrlParams(dataurl,filtercriteria);
    combinedHeaders = replaceHeaderParams(combinedHeaders,filtercriteria);

    var options = {
        method: entitydetail.method,
        url: dataurl,
        headers: combinedHeaders,
    };

    console.log('request options: ', options);

    if (connectiondetail.proxy && connectiondetail.proxy!='undefined') {
        options["proxy"] = connectiondetail.proxy;
    }

    request(options, function(error, response, body) {

        if (error) {
            callback({ message:'Error occured while fetching data thru REST API connector...', details: {error: error, response: response}}, null);
        }
        else {
            if (maxrows>0 && typeof body === 'object' 
                    && !commonMethods.isEmpty(body) 
                    && body.length>0 && body.length>maxrows) {
                
                var result = [];
                for(var i=0; i<maxrows;i++) {
                    result.push(body[i]);
                }        
                callback(null, result);
            }
            else {
                callback(null, body);
            }
            
        }

    });
        
};

function getSessionToken(entityinfo, callback) 
{

    if (!commonMethods.isEmpty(entityinfo.connector.connectiondetail.sessioninfo) &&
        !commonMethods.isEmpty(entityinfo.connector.connectiondetail.sessioninfo.options) &&
        !commonMethods.isEmpty(entityinfo.connector.connectiondetail.sessioninfo.tokenkey)) {

        var options = entityinfo.connector.connectiondetail.sessioninfo.options;
        var sessiontokenkey = entityinfo.connector.connectiondetail.sessioninfo.tokenkey;

        request(options, function(error, response, body) {
            
                    if (error) {
                        callback({ message:'Error occured while fetching session toekn thru REST API connector...',
                                     details: {error: error, response: response}}, null);
                    }
                    else {
                        if (body) {
                            var sessiondtl = JSON.parse(body);
                            var sessionhdr = {};
                            sessionhdr[sessiontokenkey.outkey] = sessiondtl[sessiontokenkey.inkey];
                            callback(null, sessionhdr);
                        }
                        else {
                            callback({ message:'Error occured while fetching session toekn thru REST API connector...response body',
                            details: {error: error, response: response, body: body}}, null);
                        }
                    }
            
        });

    }
    else {
        callback(null, {Authorization: "XXXXXX"});
    }
       
}


module.exports = {
    About: function(callback) {
                callback("This is a REST Api Connector defined to pull the data from any rest api.");
    },

    TestConnection: function(connectiondetail, callback) {
        connectDB(connectiondetail, callback);
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {
       
                    getSessionToken(entityinfo, function(sessionerr, sessionToken) {

                        if (sessionerr) {
                            callback(sessionerr,null);
                        }
                        else {

                            console.log('rest api connector sessionToken =>', sessionToken);
                            
                            if (!commonMethods.isNull(sessionToken)) {
                                
                                    getResponse(entityinfo, sessionToken, filtercriteria, maxrows,
                                        function(err, data) {
                                            if (err) {
                                                callback(err,null);   
                                            }   
                                            else {
                                                callback(null,data);
                                            }
                                    });
                                }
                                else {
                                    callback({message:'SessionToken is not extracted',
                                            details: {"entityinfo": entityinfo,
                                                        "filtercriteria": filtercriteria}  
                                            },null);
                                }
                        }
                    });

                    

                    
    }         


}