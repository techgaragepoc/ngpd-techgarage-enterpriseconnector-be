function replaceAll(source, search, replacement) {
    return source.replace(new RegExp(search, 'g'), replacement);
}

module.exports = {
                
    GetInsance: function(entityinfo, callback) {
                    if (entityinfo && entityinfo!='undefined' &&
                            entityinfo.connectortype && entityinfo.connectortype != 'undefined' &&
                            entityinfo.connectortype.provider && entityinfo.connectortype.provider !='undefined') {
                    //based on connector type, call the connector and pass the information
                    var connector = require('./' + entityinfo.connectortype.provider);
                    
                    if (connector && connector!='undefined') {
                        connector.About(function(msg){console.log('About connector - ', msg);});
                        callback(null,connector);
                    }
                    else {
                        return callback({message: "Connector not found", entityinfo: entityinfo},null);
                    }    
                }
                else {
                    return callback({message: "Connector definition not appropriate in entityinfo", entityinfo: entityinfo},null);
                }

    },

   
    GetConditionColumns: function(filtercriteria) {
                    var conditioncols = [];
                
                    if (filtercriteria!=null && filtercriteria!='undefined' && typeof filtercriteria === 'object') {
                        for(var i=0; i<Object.keys(filtercriteria).length; i++){
                
                            var key = Object.keys(filtercriteria)[i];
                            var value = filtercriteria[key];
                            //console.log(key, value);
                
                            if (typeof value === 'object') {
                
                                for(var j=0; j<Object.keys(value).length; j++){
                                    var subkey = Object.keys(value)[j];
                                    var subvalue = value[subkey];
                                    var suboperator = "=";

                                    switch(subkey.toLowerCase()) {
                                        case "$eq": suboperator = "="; break;
                                        case "$ne": suboperator = "<>"; break;
                                        case "$gt": suboperator = ">"; break;
                                        case "$gte": suboperator = ">="; break;
                                        case "$lt": suboperator = "<"; break;   
                                        case "$lte": suboperator = "<="; break;   
                                        case "$lk": suboperator = "like"; break;  
                                        case "$in": suboperator = "in"; break;
                                        case "$nin": suboperator = "not in"; break;
                                    }

                                    conditioncols.push({"column": key, "operation": suboperator, "value": subvalue});
                                }
                            }
                            else {
                                conditioncols.push({"column": key, "operation": "=", "value": value});
                            }
                    
                        }   
                  }
                
                    return conditioncols;
                },

    buildSQLCondition:  function(filtercriteria) {
                    
                        var conditions = this.GetConditionColumns(filtercriteria);
                        //console.log('extracted conditions ', conditions);
                        var conditionString = "";
                    
                        if (conditions!=null && conditions!='undefined' && conditions.length>0) {
                            for(var i=0; i<conditions.length; i++) {
                                var open_value = " '";
                                var close_value = "' ";

                                if (conditionString!='') {
                                    conditionString = conditionString + " and ";
                                }
                    
                                if (conditions[i].operation == 'in' || conditions[i].operation == 'not in') {
                                    conditions[i].value = replaceAll(conditions[i].value,"[","(");
                                    conditions[i].value = replaceAll(conditions[i].value,"]",")"); 
                                    conditions[i].value = replaceAll(conditions[i].value,"\"","'");
                                    open_value = "";
                                    close_value = "";
                                }

                                conditionString = conditionString + conditions[i].column + " " + conditions[i].operation + open_value + conditions[i].value + close_value;
                            }
                        }
                    
                        return conditionString;
            },

    buildMongoCondition:  function(filtercriteria, findparams) {
                
                    var conditions = [];
                    var filterconditions = this.GetConditionColumns(filtercriteria);
                    var findconditions = this.GetConditionColumns(findparams);

                    if (filterconditions!=null && filterconditions!='undefined' && filterconditions.length>0) {
                        for(var i=0; i<filterconditions.length; i++) {
                            conditions.push(filterconditions[i]);
                        }
                    }

                    if (findconditions!=null && findconditions!='undefined' && findconditions.length>0) {
                        for(var i=0; i<findconditions.length; i++) {
                            conditions.push(findconditions[i]);
                        }
                    }

                    //console.log('extracted conditions ', conditions);
                    var findparams = {};
                
                    if (conditions!=null && conditions!='undefined' && conditions.length>0) {
                        for(var i=0; i<conditions.length; i++) {

                            switch(conditions[i].operation) {

                              case '=':    findparams[conditions[i].column] = {"$eq": conditions[i].value}; break;
                              case '<>':    findparams[conditions[i].column] = {"$ne": conditions[i].value}; break;
                              case '>':    findparams[conditions[i].column] =  {"$gt": conditions[i].value}; break;
                              case '>=':    findparams[conditions[i].column] =  {"$gte": conditions[i].value}; break;
                              case '<':    findparams[conditions[i].column] =  {"$lt": conditions[i].value}; break;
                              case '<=':    findparams[conditions[i].column] =  {"$lte": conditions[i].value}; break;
                              case 'like': findparams[conditions[i].column] =  {"$regex": conditions[i].value, '$options': 'i'};
                              case 'in':    findparams[conditions[i].column] =  {"$in": conditions[i].value}; break;
                              case 'not in':    findparams[conditions[i].column] =  {"$nin": conditions[i].value}; break;
                            }
                        }
                    }
                
                    return findparams;
        },            

}