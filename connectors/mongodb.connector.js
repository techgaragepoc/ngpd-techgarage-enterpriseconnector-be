
const MongoClient = require('mongodb').MongoClient;
const baseConnector = require('./base.connector');
const commonMethods = require('../lib/commonmethods.js');

// connect to your database and get documents
function connectDB(connectiondetail, callback) {
    
        var dbConfig = {
            user: connectiondetail.user,
            password: connectiondetail.password,
            dbname: connectiondetail.dbname,
            url: connectiondetail.url,
        };
    
        // Use connect method to connect to the server
        MongoClient.connect(dbConfig.url, function(err, client) {
                if (err) {
                    callback({
                        "success": false,
                        "error": err
                    });
                }
                else {
                    callback({
                        "success": true,
                        "error": null
                    });
                }
        });        
}


// connect to your database and get documents
function getDocuments(connectiondetail, operation, querydetail, callback) {

    var dbConfig = {
        user: connectiondetail.user,
        password: connectiondetail.password,
        dbname: connectiondetail.dbname,
        url: connectiondetail.url,
    };

    // Use connect method to connect to the server
    MongoClient.connect(dbConfig.url, function(err, client) {
        
            console.log("Connected successfully to server");
        
            const db = client.db(dbConfig.dbname);

            if (operation == "find") {
                // Get the documents collection
                const collection = db.collection(querydetail.collection);
                // Find some documents
                collection.find(querydetail.findparams).limit(querydetail.maxrows).toArray(function(err, docs) {
                    console.log("Found the following records");
                    console.log(docs);
                    callback(null, docs);
                });
            }
            else {
                callback({ message:'Error - only find/get operation is allowed', details: operation}, null);
            }
        
            client.close();
    });
        
};

function getQueryDetail(entitydetail, findparams, maxrows) 
{
        var querydetail = {
                            collection: "",
                            findparams: "",
                            maxrows: maxrows
                        };

        if (!commonMethods.isNull(entitydetail.source.collection)) {
            querydetail.collection = entitydetail.source.collection;

            querydetail.findparams = baseConnector.buildMongoCondition(entitydetail.source.filtercriteria,findparams);
        }
        
        return querydetail;
}


module.exports = {
    About: function(callback) {
                callback("This is a MongoDB Connector defined to pull the data from any mongodb database.");
    },

    TestConnection: function(connectiondetail, callback) {
        connectDB(connectiondetail, callback);
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {
       
                    var querydetail = getQueryDetail(entityinfo.dataset, filtercriteria, maxrows);

                    console.log('mongo connector querydetail =>', querydetail);

                    if (!commonMethods.isNull(querydetail) && !commonMethods.isNull(querydetail.collection)) {
                      
                        getDocuments(entityinfo.connector.connectiondetail, "find", querydetail, 
                             function(err, data) {
                                if (err) {
                                    callback(err,null);   
                                }   
                                else {
                                    callback(null,data);
                                }
                        });
                    }
                    else {
                        callback({message:'Collection is not defined',
                                  details: {"entityinfo": entityinfo,
                                            "filtercriteria": filtercriteria}  
                                },null);
                    }
    }         


}