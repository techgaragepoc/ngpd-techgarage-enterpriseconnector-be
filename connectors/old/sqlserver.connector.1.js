
const sqlServer = require("mssql");
const baseConnector = require('./base.connector');
const commonMethods = require('../lib/commonmethods.js');

// connect to your database
function executeSQL(connectiondetail, sqlQuery, callback) {

    var dbConfig = {
        user: connectiondetail.user,
        password: connectiondetail.password,
        server: connectiondetail.server, 
        database: connectiondetail.database,
        port: connectiondetail.port,
        options: {
            //encrypt: false, // Use this if you're on Windows Azure
            instanceName: connectiondetail.instancename
          }
    };

    sqlServer.close();
    sqlServer.connect(dbConfig, function (err) {
    
        if (err) {
            console.log(err);
            callback({ message:'Error while establishing a connection with SQL Server', details: err}, null);
        }
        
        // create Request object
        var request = new sqlServer.Request();

        if (sqlQuery == '') {

            callback({ message:'Blank SQL String', details: null}, null);
        }
        else {

            // query to the database and get the records
            request.query(sqlQuery, function (err, data) {
                
                if (err) { 
                    console.log(err);
                    callback({ message:'Error while executing sql on SQL Server', details: sqlQuery}, null);
                }

                // send records as a response
                //console.log(data);

                if (!data || data=='undefined') {
                    callback({ message:'Error while executing sql on SQL Server', details: data}, null);
                }
                else {
                    callback(null,data.recordset);    
                }
                

            });
        }    
    });
};

function getSQL(entitydetail, filtercriteria, maxrows) 
{
    console.log(entitydetail, filtercriteria, maxrows);
    var sqlstring = "";

    if (!commonMethods.isNull(entitydetail.source.sql)) {
    sqlstring = entitydetail.sql;
    }
    else {

    if (!commonMethods.isNull(entitydetail.source.table)) {

        sqlstring = "SELECT ";

        if (!commonMethods.isNull(maxrows)) {
            sqlstring = sqlstring + "TOP " + maxrows + " ";
        }

        if (!commonMethods.isNull(entitydetail.source.columns)) { 
            sqlstring = sqlstring + entitydetail.source.columns;
        }
        else {
            sqlstring = sqlstring + " * ";
        }

        sqlstring = sqlstring + " FROM " + entitydetail.source.table;
        
        if (!commonMethods.isNull(filtercriteria)) {
            var conditioncols = baseConnector.buildSQLCondition(filtercriteria);
            if (!commonMethods.isNull(conditioncols)) {
                sqlstring = sqlstring + " WHERE " + conditioncols;
            }
        }
    }
    }
    
    return sqlstring;
 }
    

module.exports = {
    About: function(callback) {
                callback("This is SQL Server Connector defined to pull the data from MS SQL Server.");
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {
       
                    var sqlstring = getSQL(entityinfo.dataset, filtercriteria, maxrows);

                    if (!commonMethods.isNull(sqlstring)) {
                      
                        executeSQL(entityinfo.connector.connectiondetail, sqlstring, function(err, data) {

                             if (err) {
                                 callback(err,null);   
                             }   
                             else {
                                 callback(null,data);
                             }
                        });
                    }
                    else {
                        callback({message:'SQL String is formed as blank',
                                  details: {"entityinfo": entityinfo,
                                            "filtercriteria": filtercriteria}  
                                },null);
                    }
    }         


}