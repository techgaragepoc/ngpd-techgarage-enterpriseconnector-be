
const oracledb = require('oracledb');
const baseConnector = require('./base.connector');
const commonMethods = require('../lib/commonmethods.js');

// connect to your database
function executeSQL(connectiondetail, sqlQuery, callback) {

    var dbConfig = {
        user: connectiondetail.user,
        password: connectiondetail.password,
        connectString: connectiondetail.connectstring, 
    };


    oracledb.getConnection(dbConfig,
        function (err, connection) {
            if (err) {
                console.error(err.message);
                callback({ message:'Error while establishing a connection with Oracle', details: err}, null);
                return;
            }

            var sql = sqlQuery;

            if (connection.oracleServerVersion >= 1201000000) {
              // 12c row-limiting syntax
              sql += " OFFSET :offset ROWS FETCH NEXT :maxnumrows ROWS ONLY";
            } else {
              // Pre-12c syntax [could also customize the original query and use row_number()]
              sql = "SELECT * FROM (SELECT A.*, ROWNUM AS MY_RNUM FROM "
                  + "(" + sql + ") A "
                  + "WHERE ROWNUM <= :maxnumrows + :offset) WHERE MY_RNUM > :offset";
            }
        
            connection.execute(
              sql,
              { offset: 0, maxnumrows: 150 },
              { maxRows: 150, outFormat : oracledb.OBJECT  },
              function(err, result) {
                if (err) {
                  console.error(err.message);
                  doRelease(connection);
                  callback({ message:'Error while executing sql on Oracle', details: sql}, null);
                } else {
                  console.log("Executed: " + sql);
                  console.log("Number of rows returned: " + result.rows.length);
                  console.log(result.rows);
                  doRelease(connection);
                  callback(null,result.rows);
                }
              });

    });    
  
};

// connect to your database
function connectDB(connectiondetail, callback) {
    
        var dbConfig = {
            user: connectiondetail.user,
            password: connectiondetail.password,
            connectString: connectiondetail.connectstring, 
        };
    
    
        oracledb.getConnection(dbConfig,
            function (err, connection) {
                if (err) {
                    
                    callback({
                        "success": false,
                        "error": err
                    });
                }
                else {
                    doRelease(connection);
                    callback({
                        "success": true,
                        "error": null
                    });
                }
    
            });
 }

// connect to your database
function executeProcedure(connectiondetail, procedurename, procparams, filtercriteria, maxrows, callback) {
    
        var dbConfig = {
            user: connectiondetail.user,
            password: connectiondetail.password,
            connectString: connectiondetail.connectstring, 
        };
    
    
        oracledb.getConnection(dbConfig,
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    callback({ message:'Error while establishing a connection with Oracle', details: err}, null);
                    return;
                }
    
                var params = {};
                var outputvar = 'out_cursor';
                var sql = 'BEGIN ' + procedurename + '(';
                for(var i=0;i< Object.keys(filtercriteria).length; i++) {
                    var key = Object.keys(filtercriteria)[i];
                    var val = filtercriteria[key];

                    sql = sql + ':' + key + ',';
                    params[key] = val;
                }
                
                sql = sql + ':' + outputvar +'); END;'
                params[outputvar] = { "type": oracledb.CURSOR, "dir": oracledb.BIND_OUT };
            
                console.log(sql, params);
            
                connection.execute(
                    sql,
                    params,
                  function(err, result) {
                    if (err) {
                      console.error(err.message);
                      doRelease(connection);
                      callback({ message:'Error while executing proc on Oracle', details: sql}, null);
                    } else {
                       var resultSet =  result.outBinds[outputvar];
                       //console.log(result, resultSet);

                      resultSet.getRows(maxrows, 
                            function (cerr, rows)
                                {
                                    if (rows) {
                                       
                                        callback(null,rows);     
                                    }
                                    else {
                                        doRelease(connection);
                                        callback({ message:'Error while executing proc on Oracle, result.rows is null', details: rows}, null);
                                    }
                            });
                    }
                  });
        });    
      
    };
    

function doRelease(connection) {
    connection.close(
      function(err) {
        if (err) {
          console.error(err.message);
        }
      });
  }

function getSQL(entitydetail, filtercriteria, maxrows) {
    
    var sqlstring = "";
    var conditioncols = "";

    if (!commonMethods.isNull(entitydetail.source.sql)) {
    sqlstring = entitydetail.sql;
    }
    else {

    if (!commonMethods.isNull(entitydetail.source.table)) {

        sqlstring = "SELECT ";

        
        if (!commonMethods.isNull(entitydetail.source.columns)) { 
            sqlstring = sqlstring + entitydetail.source.columns;
        }
        else {
            sqlstring = sqlstring + " * ";
        }

        sqlstring = sqlstring + " FROM " + entitydetail.source.table + " WHERE ";
        
        if (!commonMethods.isNull(entitydetail.source.filter)) {
            sqlstring = sqlstring + entitydetail.source.filter;
        }
        else {
            sqlstring = sqlstring + " 1=1 ";
        }
        
        if (!commonMethods.isNull(filtercriteria)) {
            conditioncols = baseConnector.buildSQLCondition(filtercriteria);
            if (!commonMethods.isNull(conditioncols)) {
                sqlstring = sqlstring + " and " + conditioncols;
            }
        }

        if (!commonMethods.isNull(maxrows)) {
            sqlstring = sqlstring + " and rownum <= " + maxrows + " ";
        }
    }
 }
    
    return sqlstring;
}

module.exports = {
    About: function(callback) {
                callback("This is Oracle Connector defined to pull the data from Oracle.");
    },

    TestConnection: function(connectiondetail, callback) {
        connectDB(connectiondetail, callback);
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {
       
                    var sqlstring = getSQL(entityinfo.dataset, filtercriteria, maxrows);

                    if (!commonMethods.isNull(sqlstring)) {
                      
                        executeSQL(entityinfo.connector.connectiondetail, sqlstring, function(err, data) {

                             if (err) {
                                 callback(err,null);   
                             }   
                             else {
                                 callback(null,data);
                             }
                        });
                    }
                    else {

                        if (!commonMethods.isNull(entityinfo.dataset.source) && 
                            !commonMethods.isNull(entityinfo.dataset.source.procedure) && 
                            !commonMethods.isNull(entityinfo.dataset.source.procedure.name)) {
                            
                              executeProcedure(entityinfo.connector.connectiondetail, 
                                         entityinfo.dataset.source.procedure.name, 
                                         entityinfo.dataset.source.procedure.parameters, 
                                         filtercriteria, maxrows,
                                         function(err, data) {
      
                                   if (err) {
                                       callback(err,null);   
                                   }   
                                   else {
                                       callback(null,data);
                                   }
                              });
                          }
                          else {

                            callback({message:'SQL String is formed as blank',
                                    details: {"entityinfo": entityinfo,
                                                "filtercriteria": filtercriteria}  
                                    },null);
                          }
                    }
    }         


}