
const soap = require('soap');
const request = require('request');
const baseConnector = require('./base.connector');
const commonMethods = require('../lib/commonmethods.js');

function getAbsoluteUrl(baseurl, relativeurl) {

    var url = baseurl;

    if (baseurl.substr(-1) == '/') {
        url = baseurl.substr(0,baseurl.length-1);
    }

    if (relativeurl.length>0) {
        if (relativeurl.substr(0,1) == '/') {
            url = url + relativeurl;
        }
        else {
            url = url + '/' + relativeurl;
        }
    } 

    //remove last /
    if (url.substr(-1) == '/') {
        url = url.substr(0,baseurl.length-1);
    }
    
    return url;

}


function replaceUrlParams(source, filtercriteria) {
    if (typeof filtercriteria === 'object' && !commonMethods.isEmpty(filtercriteria)) { 
            //&& typeof filtercriteria.url === 'object' && !commonMethods.isEmpty(filtercriteria.url)) {
            var urlParams = filtercriteria; //filtercriteria.url;

            for(var i=0; i<Object.keys(urlParams).length;i++) {
                var key = Object.keys(urlParams)[i];
                var param = '#'+key + '#';
                if (source.indexOf(param) > -1) {
                    source = commonMethods.replaceAll(source, param, urlParams[key]);
                }
            }
    }

    return source;
}

function replaceArgumentParams(source, filtercriteria) {
    
    var target = source;

    if (typeof filtercriteria === 'object' && !commonMethods.isEmpty(filtercriteria)) {
       
        if (
          // typeof filtercriteria.arguments === 'object' && !commonMethods.isEmpty(filtercriteria.arguments) &&
              typeof source === 'object' && !commonMethods.isEmpty(source)) {

                var sourceAsString = JSON.stringify(source);
                var args = filtercriteria; //filtercriteria.arguments;
            
                for(var i=0; i<Object.keys(args).length;i++) {
                    var key = Object.keys(args)[i];
                    var val = args[key];
                    var param = '#'+key + '#';

                    if (sourceAsString.indexOf(param) > -1) {
                        sourceAsString = commonMethods.replaceAll(sourceAsString, param, val);
                    }
                }

                target = JSON.parse(sourceAsString);

        } //for headers

     }

     return target;
}


function connectDB(connectiondetail, callback) {
    
        console.log('connectiondetail ', connectiondetail);   

        var dataurl = getAbsoluteUrl(connectiondetail.baseurl,connectiondetail.testmethodurl);
        var wsdlurl = dataurl + '?wsdl';
      //  var userId = connectiondetail.credentials.userid;
      //  var password = connectiondetail.credentials.password;
       // var methodname = entitydetail.methodname;
        
        var soap_client_options = {
            endpoint: dataurl
        };
    
        if (connectiondetail.proxy && connectiondetail.proxy!='undefined') {
            soap_client_options["request"] = request.defaults({
                                                'proxy': connectiondetail.proxy,
                                                'timeout': 60000,
                                                'connection': 'keep-alive'});
        }
    
        console.log('url, options, connectiondetail',wsdlurl, soap_client_options, connectiondetail);
        soap.createClient(wsdlurl, soap_client_options, function(err, client) {
    
            if (err) {
                console.log('soap connector connection error:', err);
                callback({
                    "success": false,
                    "error": err
                });
            }
            else {
                callback({
                    "success": true,
                    "error": null
                });
            }
        });
 }    

// connect to your database and get documents
function getResponse(entityinfo, filtercriteria, maxrows, callback) {

    var connectiondetail = entityinfo.connector.connectiondetail;
    var entitydetail = entityinfo.dataset.source;
   
    var dataurl = getAbsoluteUrl(connectiondetail.baseurl, entitydetail.url);
    dataurl = replaceUrlParams(dataurl,filtercriteria);

    var wsdlurl = dataurl + '?wsdl';

    var args = replaceArgumentParams(entitydetail.arguments,filtercriteria);

    console.log('connectiondetail ', connectiondetail);
    var userId = connectiondetail.credentials.userid;
    var password = connectiondetail.credentials.password;
    var methodname = entitydetail.methodname;
    
    var soap_client_options = {
        endpoint: dataurl
    };

    if (connectiondetail.proxy && connectiondetail.proxy!='undefined') {
        soap_client_options["request"] = request.defaults({
                                            'proxy': connectiondetail.proxy,
                                            'timeout': 60000,
                                            'connection': 'keep-alive'});
    }

    console.log('url, options, connectiondetail',wsdlurl, soap_client_options, connectiondetail);
    soap.createClient(wsdlurl, soap_client_options, function(err, client) {

        if (err) {
            console.log('Error occured while creating soap client', err);
            callback({ message:'Error occured while creating soap client thru SOAP API connector...',
            details: {error: err, response: null}}, null);
        }
        else {
            var passwordEscape = password;
            passwordEscape = passwordEscape.replace("\'","'").replace("\\","\\\\");
         //  userId = "ISU_Chatbot@cpsg_dpt1";
         //  password = "PH\\\\_h3rP'98jz%4Y";
            console.log(password, passwordEscape);
            
            var wsSecurity = new soap.WSSecurity(userId,passwordEscape, {"passwordType": "PasswordText", "mustUnderstand": "1"});
            
            client.setSecurity(wsSecurity);
            //console.log('client...', client);
            var methodtoexecute = eval('client.' + methodname);
    
            methodtoexecute(args, function(err, result) {
    
                if (err) {
                    callback({ message:'Error occured while fetching data thru SOAP API connector...',
                                 details: {error: err, response: result}}, null);
                }
                else {
                    console.log(result);
                    callback(null,result);
                }
            });            
        }
    }); //soap.createClient
};



module.exports = {
    About: function(callback) {
                callback("This is a SOAP Api Connector defined to pull the data from any soap api.");
    },

    TestConnection: function(connectiondetail, callback) {
        connectDB(connectiondetail, callback);
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {
       
                   getResponse(entityinfo, filtercriteria, maxrows,
                                        function(err, data) {
                                            if (err) {
                                                callback(err,null);   
                                            }   
                                            else {
                                                callback(null,data);
                                            }
                                    });

    },
                    
}