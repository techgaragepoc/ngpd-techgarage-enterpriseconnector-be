const connectortypes = require('./json/connectortypes.json');
const connectors = require('./json/connectors.json');
const datasets = require('./json/datasets.json');
const domainentities = require('./json/domainentities.json');

module.exports = {

    GetItem: function(itemtype, itemname, callback) {
                        var items = [];
                        var item = {};
                        
                        switch(itemtype.toLowerCase()) {
                            case 'domainentity': items = domainentities; break;
                            case 'dataset': items = datasets; break;
                            case 'connector': items = connectors; break;
                            case 'connectortype': items = connectortypes; break;
                        }
                        
                        if (items && items.length>0) {
                            for(var i=0; i<items.length; i++) {
                                if (items[i].name.toLowerCase() == itemname.toLowerCase()) {
                                    item = items[i];
                                    break; 
                                }
                            }
                        }
                     
                        callback(null, item);
    },

    

}