const ec = require('../services/service');
const testdataset = require('./testdata.js');


module.exports = {

    GetConnectorList: function () {
        ec.GetConnectorList(function (err, files) {
            if (err) {
                console.log('Error occured in GetConnectorList call...', err);
            }
            else {
                console.log('******************** List of Connectors ****************');
                for (var i = 0; i < files.length; i++) {
                    console.log((i + 1) + '. ' + files[i]);
                }
            }
        });
    },

    TestConnection: function (testconnectorname) {

        var connectordtl = {};

        testdataset.GetItem('connector', testconnectorname, function (cerr, conn) {
            if (cerr) { console.log('Error occured while fetching test connector', cerr); }
            else {

                connectordtl = conn;
                testdataset.GetItem('connectortype', conn.connectortypename, function (cterr, conntyp) {
                    if (cterr) { console.log('Error occured while fetching test connector type', cterr); }
                    else {

                        connectordtl["connectortype"] = conntyp;
                        ec.TestConnection(connectordtl, 
                            function (err, data) {
                                if (err) {
                                    console.log('Error occured in TestConnection call...', err);
                                }
                                else {
                                    console.log('******************** Test Connection ****************');
                                    console.log(JSON.stringify(data));
                                }
                            });
                    }
                }); // GetItem => 'connectortype'

            }
        }); // GetItem => 'connector'    

    },


    GetData: function (testparams) {

        var entityinfo = {};

        testdataset.GetItem('domainentity', testparams.entityname, function (err, testentity) {

            if (err) { console.log('Error occured while fetching test data', err); }
            else {

                ec.GetProrityDataSet(testentity.datasets, testparams.decidingfactor, function (perr, pds) {

                    if (perr) { console.log('Error occured while prioritising test data set', perr); }
                    else {

                        testdataset.GetItem('dataset', pds.datasetname, function (dserr, ds) {
                            if (dserr) { console.log('Error occured while fetching test dataset', dserr); }
                            else {

                                entityinfo.dataset = ds;
                                testdataset.GetItem('connector', ds.connectorname, function (cerr, conn) {
                                    if (cerr) { console.log('Error occured while fetching test connector', cerr); }
                                    else {

                                        entityinfo.connector = conn;
                                        testdataset.GetItem('connectortype', conn.connectortypename, function (cterr, conntyp) {
                                            if (cterr) { console.log('Error occured while fetching test connector type', cterr); }
                                            else {

                                                entityinfo.connectortype = conntyp;
                                                ec.GetData(entityinfo, testparams.filtercriteria, testparams.maxrows,
                                                    function (err, data) {
                                                        if (err) {
                                                            console.log('Error occured in GetData call...', err);
                                                        }
                                                        else {
                                                            console.log('******************** Data ****************');
                                                            console.log(JSON.stringify(data));
                                                        }
                                                    });
                                            }
                                        }); // GetItem => 'connectortype'

                                    }
                                }); // GetItem => 'connector'    

                            }
                        }); // GetItem => 'dataset'

                    }
                }); //Get Prioritise dataset

            }
        });  // GetItem => 'domainentity'

    },

}



