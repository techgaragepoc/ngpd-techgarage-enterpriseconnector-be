const express = require("express");
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const multer = require('multer');


const appconfig = require('./config');

const index = require('./routes/index.route.js');
const econnector = require('./routes/econnector.route.js');


var app = express();
var corsOptions = {
    origin: function (origin, callback) {
                console.log(origin);
                if (origin === undefined || appconfig.endpointfe.indexOf(origin) !== -1) {
                    callback(null, true)
                } else {
                    callback(new Error('Not allowed by CORS'))
                }
            },
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    allowedHeaders: 'Origin, X-Requested-With, Content-Type, Content-Length, Accept, enctype,auth-secretid,auth-secrettoken',
    credentials: true,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
  }


app.use(cors(corsOptions));
app.use(cookieParser());
//app.use(express.static('userfiles'));
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(multer({ dest: 'userfiles/tmp/'}).array('document'));

// parse various different custom JSON types as JSON
app.use(bodyParser.json())


app.use(function(req, res, next) {
   // console.log('hdr');
   var reqorigin = req.headers['origin'];
   if (appconfig.endpointfe.indexOf(reqorigin) <= -1) {
       reqorigin = appconfig.endpointfe[0];
   }    
  
   res.header("Access-Control-Allow-Origin", reqorigin);
   // res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Content-Length, Accept, enctype,auth-secretid,auth-secrettoken");
    res.header("Access-Control-Allow-Credentials","true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE");
    next();
});


//solving the header send problem
app.use(function(req, res, next) {
    console.log('header send prob');
    var _send = res.send;
    var sent = false;
    console.log();
    res.send = function(data) {
        if (sent) return;
        _send.bind(res)(data);
        sent = true;
    };
    next();
});

app.use(function(err, req, res, next) {
    // Do logging and user-friendly error message display
    console.error(err);
     res.status(500).send('internal server error');
});


app.use('/',index);
app.use('/econnector',econnector);

var PORT = process.env.PORT || appconfig.port;
var server = app.listen(PORT,function(){
    var host = server.address().address;
    var port = server.address().port;

    if (host == null || host=='undefined' || host =='::')
        host = 'localhost';

    //logService.Save('info','Knowledge Base Service started',{}); 
    console.log('Enterprise Connector REST API listening at http://%s:%s/',host,port);
});

