
const fs = require('fs');
const path = require('path');

const connectorsdir = __dirname + '/../connectors';
const prioritiesSource = require('../lib/prioritiessource.js');
const commonMethods = require('../lib/commonmethods.js');


module.exports = {

    GetConnectorList: function(callback) {
        var connectors = [];
        console.log(connectorsdir);
        fs.readdir(connectorsdir, (err, files) => {
            if (err) {
                callback({error: err}, null);
            }
            else {
                for(var i=0;i<files.length; i++) {
                    if ((files[i].toLowerCase() != 'base.connector.js')  && 
                         (files[i].toLowerCase().indexOf('.connector.') > -1)) {
                        connectors.push(files[i]);
                    }
                }
                callback(null, connectors);
            }

          });
    },

    TestConnection: function(connectordtl, callback) {
        const baseconnector = require(connectorsdir + '/base.connector');
         console.log('inside data service (testconnection) - ', connectordtl);

        var entityinfo = {};
        entityinfo["connectortype"] = connectordtl.connectortype;
        
        if (baseconnector && baseconnector!='undefined') {
            console.log('specific instance created...'); 
            baseconnector.GetInsance(entityinfo, function(connerr, connector) {

                 if (connerr) {   
                     console.log('get instance error: ', connerr);
                     callback(connerr, null); 
                 } 
                 else {
                    console.log('establishing connection...');
                    connector.TestConnection(connectordtl.connectiondetail, function(response) {
                      if (response) {
                          response["connectioninfo"] = connectordtl;
                      }  
                      callback(null, response);
                    });  

                 }    
            });
        }
    },

    GetProrityDataSet: function(datasets, decidingfactor, callback) {
       
        prioritiesSource.GetProrityDataSet(datasets, decidingfactor, function(err, ds) {
            if (err) {
                callback({error: err}, null);
            }
            else {
                callback(null, ds);
            }
        });
    },

    GetData: function(entityinfo, filtercriteria, maxrows, callback) {

              const baseconnector = require(connectorsdir + '/base.connector');
               // console.log('inside data service (entityinfo) - ', entityinfo);

               if (baseconnector && baseconnector!='undefined') {
                    
                    baseconnector.GetInsance(entityinfo, function(connerr, connector) {

                        if (connerr) {   
                            callback(connerr, null); 
                        } 
                        else {
                            //console.log(entityinfo,filtercriteria,maxrows);
                            if (!commonMethods.isNull(entityinfo)) {

                                if (!commonMethods.isNull(entityinfo.dataset) 
                                    && !commonMethods.isNull(entityinfo.dataset.source))  {
                                        entityinfo.dataset.source = commonMethods.AsObject(entityinfo.dataset.source);
                                    }    

                                if (!commonMethods.isNull(entityinfo.connector) 
                                    && !commonMethods.isNull(entityinfo.connector.connectiondetail))  {
                                        entityinfo.connector.connectiondetail = commonMethods.AsObject(entityinfo.connector.connectiondetail);
                                    }
                            }    

                            connector.GetData(entityinfo, filtercriteria, maxrows, function(err,data) {
                                if (err) { 
                                    console.log(err);
                                    callback(err, null); 
                                }
                                else {
                                    //return the result set
                               
                                    callback(null, data);
                                }
                            });    
                        }
                    })
                }
                else {

                    //no connector found
                    callback({message: "Base connector not found", error: null}, null);
                }
    }
}