const express = require('express');
const router = express.Router();
const commonMethods = require('../lib/commonmethods.js');

var authUser = {};

/* GET home page. */
router.get('/', function (req, res, next) {

  
      if (req.header('Content-Type') == 'application/json') {
        return res.json({ message: "unqualified path...please check the url" });
      } else {
        return res.send('Welcome to Enterprise Connector API');
      }
  
});

module.exports = router;
