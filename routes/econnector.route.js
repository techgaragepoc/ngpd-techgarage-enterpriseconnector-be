const express = require('express');
const router = express.Router();
const econnectorCtrl = require('../controllers/econnector.controller');


router.get('/', function (req, res) {
    res.status(200).send({message:"Enterprise Connector REST API: Please specify the method"});
});

router.get('/connectorlist', function (req, res) {
    econnectorCtrl.GetProviderList(req, res);
});

router.post('/testconnection', function (req, res) {
    econnectorCtrl.TestConnection(req, res);
});

router.post('/prioritydataset', function (req, res) {
    econnectorCtrl.GetPriorityDataSet(req, res);
});

router.post('/data', function (req, res) {
    econnectorCtrl.GetData(req, res);
});


module.exports = router;
