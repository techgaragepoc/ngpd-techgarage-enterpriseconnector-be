const commonMethods = require('./commonmethods.js');

module.exports = {
    IsEqual: function(sourceval, targetval) {
                var matched = false;
                
                if (typeof sourceval === 'object' || commonMethods.isNull(sourceval)) {
                    console.log('source value is neither expected as object nor blank');
                }
                else {

                    if (typeof targetval !== 'object' && !commonMethods.isNull(targetval)
                        && sourceval.toLowerCase() == targetval.toLowerCase() ) {
                        matched = true;
                    }    
                    else {
                    
                        if (typeof targetval === 'object' && !commonMethods.isNull(targetval))
                            {
                                var execresult = [];
                                for(var i=0;i<Object.keys(targetval).length;i++) {
                    
                                    var trgsubkey = Object.keys(targetval)[i];
                                    var trgsubval = targetval[trgsubkey];
                                    console.log('target matching subkey, subval, srcval..', trgsubkey, trgsubval, sourceval);
                                    switch(trgsubkey) {

                                        case '$eq': execresult.push({"key": trgsubkey, "result": this.IsEqual(sourceval, trgsubval)}); 
                                                    break;

                                        case '$neq': execresult.push({"key": trgsubkey, "result": !this.IsEqual(sourceval, trgsubval)}); 
                                                    break;

                                        case '$in': execresult.push({"key": trgsubkey, "result": this.IsExistInList(sourceval, trgsubval)}); 
                                                    break;
                                                    
                                        case '$nin': execresult.push({"key": trgsubkey, "result": !this.IsExistInList(sourceval, trgsubval)}); 
                                                    break;
                                                    
                                        case '$gt': execresult.push({"key": trgsubkey, "result": this.IsGreater(sourceval, trgsubval)}); 
                                                    break;

                                        case '$gte': execresult.push({"key": trgsubkey, "result": this.IsGreaterEqual(sourceval, trgsubval)}); 
                                                    break;                                                    

                                        case '$lt': execresult.push({"key": trgsubkey, "result": this.IsLess(sourceval, trgsubval)}); 
                                                    break;

                                        case '$lte': execresult.push({"key": trgsubkey, "result": this.IsLessEqual(sourceval, trgsubval)}); 
                                                    break;                                                    

                                        case '$lk': execresult.push({"key": trgsubkey, "result": this.IsLike(sourceval, trgsubval)}); 
                                                    break;                                                    
                                                    
                                                    
                                    }
                                }

                                for(var i=0; i<execresult.length; i++) {
                                    if (execresult[i].result) {
                                        matched = true;
                                    }
                                }

                            }
                    }
                }    
       
                return matched;
    },

/*
    IsEqual: function(source, target) {
                    if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                            && source.toLowerCase() == target.toLowerCase()) {
                        return true;        
                    }
                    else { return false; }
    },
*/
    
    IsGreater: function(source, target) {
        if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                && eval(source) > eval(target)) {
            return true;        
        }
        else { return false; }
    },

    IsGreaterEqual: function(source, target) {
        if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                && eval(source) >= eval(target)) {
            return true;        
        }
        else { return false; }
    },

    IsLess: function(source, target) {
        if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                && eval(source) < eval(target)) {
            return true;        
        }
        else { return false; }
    },

    IsLessEqual: function(source, target) {
        if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                && eval(source) <= eval(target)) {
            return true;        
        }
        else { return false; }
    },

    IsLike: function(source, target) {
        if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                &&  source.toLowerCase().indexOf(target.toLowerCase()) > -1) {
            return true;        
        }
        else { return false; }
    },

    IsExistInList: function(source, target) {
        var result = false;
        if (!commonMethods.isNull(source) && !commonMethods.isNull(target) 
                && target.length>0) {

                for(var i =0; i<target.length; i++) {
                    if (this.IsEqual(target[i],source)) {
                        result = true;
                    }
                }    
        }
        else { result = false; }

        return result;
    },
}