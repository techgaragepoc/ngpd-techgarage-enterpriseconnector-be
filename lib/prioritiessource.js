const commonMethods = require('./commonmethods.js');
const compareMethods = require('./comparemethods.js');




function getProbabilityMatch(ds_df, df) {

  var total_prop = 0;
  var prop_found = 0;
  var matchedvalue_prop = 0;    

  if (typeof ds_df === 'object' && !commonMethods.isNull(ds_df)
        && typeof df === 'object' && !commonMethods.isNull(df)
        && Object.keys(df).length > 0)
    {

        total_prop = Object.keys(df).length;

        for(var i=0; i<Object.keys(df).length; i++) {

            for(var j=0; j<Object.keys(ds_df).length; j++) {

                if (Object.keys(ds_df)[j].toLowerCase() == Object.keys(df)[i].toLowerCase()) {

                     prop_found++;
                     if (compareMethods.IsEqual(df[Object.keys(df)[i]], ds_df[Object.keys(ds_df)[j]])) {
                        matchedvalue_prop++;
                     }

                } //if key matched
            } //for ds_df
        } //for df

        return (matchedvalue_prop/total_prop);
    }
  
    return 0;
}

module.exports = {

    GetProrityDataSet: function(datasets, decidingfactor, callback) { 
                        var datasetprob = [];
               
                        if (datasets && datasets.length>0) {
               
                            if (typeof decidingfactor === 'object' && !commonMethods.isNull(decidingfactor)) {
                                 for(var i=0;i<datasets.length;i++) {
                                    prob = getProbabilityMatch(datasets[i].decidingfactor, decidingfactor);
                                    datasetprob.push({index: i, probability: prob});
                                }           

                                console.log('prob of datasets', datasetprob);

                                var mostprobablematchindex = 0;
                                for(var i=1;i<datasets.length;i++) { 
                                    if (datasetprob[i].probability > datasetprob[mostprobablematchindex].probability) {
                                        mostprobablematchindex = i;
                                    }
                                }

                                callback(null, datasets[mostprobablematchindex]);

                            }
                            else {
                                console.log('deciding factor is not sent, returning default dataset');
                                callback(null,datasets[0]);
                            }
                        }
                    
        
     }
}