module.exports = {

    isNull: function(value) {

                if (value == null || value == '' || value == 'undefined' ||
                         (typeof value === 'object' && this.isEmpty(value))) { 
                    return true;
                }
                else {
                    return false;
                }
    },

    isEmpty: function(obj) {
                for(var key in obj) {
                    if(obj.hasOwnProperty(key))
                        return false;
                }
                return true;
            },

    replaceAll: function(source, search, replacement) {
                    return source.replace(new RegExp(search, 'g'), replacement);
    },

    AsObject: function(body) {
                    var bodyobj = {};
                    
                    if (body && typeof body !== 'object') {
                        bodyobj = JSON.parse(body);
                    }
                    else {
                        bodyobj = body;
                    }
                
                    return bodyobj;
    },

    addPropertyValuePair:  function(sourceObj, propvaluetoadd) {
                
                                if (typeof propvaluetoadd === 'object' && !this.isEmpty(propvaluetoadd)) {
                            
                                    for(var i=0; i<Object.keys(propvaluetoadd).length;i++) {
                                        var key = Object.keys(propvaluetoadd)[i];
                                        sourceObj[key] =  propvaluetoadd[key];
                                    }
                                }
                            
                                return sourceObj;
            }        
}