const service = require('../services/service.js');

function GetRequestBody(req) {
    var reqbody = {};
    
    if (req.body && typeof req.body !== 'object') {
        reqbody = JSON.parse(req.body);
    }
    else {
        reqbody = req.body;
    }

    return reqbody;
}

module.exports = {

    GetProviderList: function(req, res) {


        service.GetConnectorList(function(err, data) {
               if (err) {
                    res.status(500).send({"message":"Enterprise Connector REST API: Error occured while fetching connector list",
                                          "error": err});
               } 
               else {
                   res.status(200).send({"message":"Successful", "data": data}); 
               }
        });

    },

    GetPriorityDataSet: function(req, res) {
        console.log('priority req...', req.body);

        var reqbody = GetRequestBody(req);
      
        service.GetProrityDataSet(reqbody.datasets, reqbody.decidingfactor,function(err, data) {
            if (err) {
                 res.status(500).send({"message":"Enterprise Connector REST API: Error occured while finding priority dataset",
                                       "error": err});
            } 
            else {
                res.status(200).send({"message":"Successful", "data": data}); 
            }
     });

    },

    TestConnection: function(req, res) {
                      console.log('connectiondetail...', req.body);
        
                      var connectioninfo = GetRequestBody(req);
              
                        service.TestConnection(connectioninfo.connectiondetail, function(err, data) {
                            if (err) {
                                res.status(500).send({"message":"Enterprise Connector TestConnection",
                                                      "error": err});
                           } 
                           else {
                               res.status(200).send(data); 
                           }  
                        });
    },
        
    GetData: function(req, res) {
        var reqbody = GetRequestBody(req);
        
        service.GetData(reqbody.entityinfo, reqbody.parameters, reqbody.maxrows, function(err, data) {
            if (err) {
                 res.status(500).send({"message":"Enterprise Connector REST API: Error occured while fetching data",
                                       "error": err});
            } 
            else {
                res.status(200).send({"message":"Successful", "data": data}); 
            }
        });

    },
                
}