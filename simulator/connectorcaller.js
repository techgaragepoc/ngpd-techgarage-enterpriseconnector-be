var request = require("request");

var reqdata = {
    "entity": "Policy",
    "parameters": {
        "duration_months": { "$lt": "13" }
    },
    "maxrows": 2
};

var options = {
    method: 'GET',
    url: 'http://localhost:8081/data/',
    headers:
    {
        'cache-control': 'no-cache',
        'content-type': 'application/json',
        reqdata: JSON.stringify(reqdata)
    }
};

request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
});
