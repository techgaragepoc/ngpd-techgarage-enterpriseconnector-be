var express = require("express");
var bodyParser = require('body-parser');
var multer  = require('multer');
var fs = require('fs');

var app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(multer({ dest: 'tmp/'}).array('file1'));

// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })


app.get('/',function(req,res){
    console.log('Connector Simultor...');
    res.send('Connector Simultor...');
});

app.get('/data',urlencodedParser,function(req,res){
    var reqdata = JSON.parse(req.headers["reqdata"]);
    var response = [];
    if (reqdata && reqdata.entity && reqdata.parameters && reqdata.maxrows) {
        response = getData(reqdata.entity, reqdata.parameters, reqdata.maxrows);
    }
   
    console.log(response);
    res.status(201).send(response);
});


var server = app.listen(8081,function(){
    var host = server.address().address;
    var port = server.address().port;
    
    console.log('Connector Simultor - listening at http://%s:%s',host,port);
});

function getData(entityname, parameters, maxrows) {
  var data = [];
  var policydata = [
    {
        policyid: 'P101',
        plan: {name: 'plan 1'},
        startfrom: '01-01-2017',
        durationmonths: 12,
        clientid: 'CL001'
    },

    {
        policyid: 'P102',
        plan: {name: 'plan 2'},
        startfrom: '01-01-2018',
        durationmonths: 12,
        clientid: 'CL002'
    },

  ];  

  data = policydata;
  return data;

}